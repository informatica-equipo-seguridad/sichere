var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index',{title:'Hurto a Residencias', descripcion: 'Acá puedes analizar el hurto a residencias cometidos en Colombia desde el año 2017' });
});

module.exports = router;
