var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
  res.render('index', { title: 'Homicidios', descripcion: 'Acá puedes analizar los homicidios cometidos en Colombia desde el año 2017' });
});

module.exports = router;
