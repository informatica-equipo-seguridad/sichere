var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Sichere', descripcion: 'Analizando el crimen y la inseguridad en Colombia a partir de los datos abiertos de la DIJIN' });
});

module.exports = router;
