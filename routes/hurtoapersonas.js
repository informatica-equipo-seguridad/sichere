var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index',{title:'Hurto a Personas', descripcion: 'Acá puedes analizar el hurto a personas cometidos en Colombia desde el año 2017' });
});

module.exports = router;
